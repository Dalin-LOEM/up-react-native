import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import RegisterScreen from './screens/RegisterScreen'
import ProfileScreen from './screens/ProfileScreen';

class App extends Component {

 
  render() {
    console.disableYellowBox = true;
    return (
      <Router>
        <Scene key="root">
          <Scene title='Register' key="RegisterScreen" component={RegisterScreen} initial={true} hideNavBar={true} />
          <Scene title='Profile' key="ProfileScreen" component={ProfileScreen} hideNavBar={false} />
        </Scene>
      </Router>
    );
  }
}

export default App