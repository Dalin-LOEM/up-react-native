import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Container, Content, Icon } from 'native-base';

class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: props.userData.firstName,
            lastname: props.userData.lastName,
            email: props.userData.email,
            phone: props.userData.phoneNumber,
            data: props.userData

        };
    }

    render() {
        const { firstname, lastname, email, phone } = this.state
        return (
            <Container  style={{flex: 1, backgroundColor:'#F2F3F4'}}>
                <Content>
                    <View style={{ height: 250, backgroundColor: '#D6E4FF', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 20 }}>
                        <View style={{ width: '30%', }}>
                            <Image source={{ uri: 'https://i.pinimg.com/564x/db/cb/b6/dbcbb6f768c93a4b833bb97ba11a6d7a.jpg' }} style={{ width: '90%', height: '50%', borderRadius: 50 }} />
                        </View>
                        <View style={{ marginLeft: 20, width: '70%' }}>
                            <Text style={{ fontSize: 18, fontWeight: '700', color: '#5D6D7E' }}>{firstname + ' ' + lastname}</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <Icon name='staro' type='AntDesign' style={{ color: '#5D6D7E', fontSize: 18 }} />
                                <Text style={{ marginLeft: 5, }}> 1.5 </Text>
                            </View>
                        </View>
                    </View>
                    <View style={{marginTop: 20, width: '90%', alignSelf: 'center'}}>
                        <View style={{height: 56,backgroundColor: '#fff', shadowColor: "#000", shadowOffset: {width: 0,	height: 1}, shadowOpacity: 0.22,shadowRadius: 2.22,elevation: 3,borderRadius: 10, flexDirection: 'row', alignItems: 'center', padding: 10}}>
                            <Icon name='user' type='AntDesign' style={{fontSize: 18, color: '#5D6D7E'}} />
                            <Text style={{ color: '#5D6D7E', marginLeft: 10 }}>{firstname + ' ' + lastname}</Text>
                        </View>
                        <View style={{height: 56,backgroundColor: '#fff', shadowColor: "#000", shadowOffset: {width: 0,	height: 1}, shadowOpacity: 0.22,shadowRadius: 2.22,elevation: 3,borderRadius: 10, flexDirection: 'row', alignItems: 'center', padding: 10, marginTop: 10}}>
                            <Icon name='email-edit-outline' type='MaterialCommunityIcons' style={{fontSize: 18, color: '#5D6D7E'}} />
                            <Text style={{ color: '#5D6D7E', marginLeft: 10 }}>{email}</Text>
                        </View>
                        <View style={{height: 56,backgroundColor: '#fff', shadowColor: "#000", shadowOffset: {width: 0,	height: 1}, shadowOpacity: 0.22,shadowRadius: 2.22,elevation: 3,borderRadius: 10, flexDirection: 'row', alignItems: 'center', padding: 10, marginTop: 10, marginBottom: 20}}>
                            <Icon name='phone' type='SimpleLineIcons' style={{fontSize: 18, color: '#5D6D7E'}} />
                            <Text style={{ color: '#5D6D7E', marginLeft: 10 }}>{phone}</Text>
                        </View>
                    </View>
                </Content>
                <View style={{marginBottom: 50}}>
                    <TouchableOpacity style={{width: '50%', alignSelf: 'center', padding: 20, backgroundColor:'#2874A6', borderRadius: 20}}>
                        <Text style={{textAlign: 'center', color: '#fff', fontWeight: '700'}}>Sign Out</Text>
                    </TouchableOpacity>
                </View>
            </Container>
        );
    }
}

export default ProfileScreen;
