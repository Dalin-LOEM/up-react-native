import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Dimensions, Platform } from 'react-native';
import { Icon, Container, Content } from 'native-base'
import { Actions } from 'react-native-router-flux';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class RegisterScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            firstName: '',
            lastName: '',
            phoneNumber: '',
            genders: [
                { gender: 'M', status: true },
                { gender: 'F', status: false },
            ],
            gender: 'Male',
            hideShow: true,

            countryCode: '+855',
            confirmResult: null,
            fullPhoneNumber: '',
            user: null,
            phoneNumber: '',
            statusPhoneInput: true,
            statusLoading: false,
            msgErrorName: false,
            msgErrorPhone: false,
            msgErrorEmail: false,
            code: "",
            isStartDateVisible: false,
            errorPass: false,
            errorLastname: false
        }
        this.firstName = "";
        this.lastName = "";
        this.phoneNumber = "";
        this.gender = "Male";
        this.password = "";
        this.email = "";
        global.phoneNumber = "",
            global.userInfo = {}
    }

    _handleSigup = () => {
        const { phoneNumber, password, email, firstName, lastName } = this.state;
        var isFullInfo = false
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var tempEmail = email.replace(/\s/g, '');

        if (tempEmail == '' || reg.test(tempEmail) === false) {
            this.setState({ msgErrorEmail: true })
        } else {
            this.setState({ msgErrorEmail: false })
        }

        if (firstName == '') {
            this.setState({ msgErrorName: true })
        } else {
            this.setState({ msgErrorName: false })
        }
        if (lastName == '') {
            this.setState({ errorLastname: true })
        } else {
            this.setState({ errorLastname: false })
        }
        if (phoneNumber == '') {
            this.setState({ msgErrorPhone: true })
        } else {
            this.setState({ msgErrorPhone: false })
        }

        if (password == '') {
            this.setState({ errorPass: true })
        } else {
            this.setState({ errorPass: false })
        }


        if (firstName != '' && phoneNumber != '' && lastName != '' && (tempEmail != '' && reg.test(tempEmail) === true) && password != '') {
            isFullInfo = true
        }

        if (isFullInfo) {
            this.firstName = firstName
            this.lastName = lastName
            this.password = password
            this.email = tempEmail
            this.phoneNumber = phoneNumber
            let dataUser = {
                firstName : this.firstName,
                lastName : this.lastName,
                password : this.password,
                email : this.email,
                phoneNumber : this.phoneNumber,
            }
            console.log(dataUser)
            Actions.ProfileScreen({userData: dataUser})
        }
    }

    _handleGender = (index) => {
        let { genders } = this.state
        genders.map((eachItem, eachIndex) => {
            if (index == eachIndex) {
                genders[eachIndex].status = true
                if (eachItem.gender == "M") {
                    this.gender = 'Male'
                    this.setState({ gender: 'Male' })
                } else {
                    this.setState({ gender: 'Female' })
                    this.gender = 'Female'
                }

            } else {
                genders[eachIndex].status = false
            }
        })
        this.setState({ genders })
    }

    renderInformationInput() {
        const { errorPass, email, password, firstName, lastName, genders, phoneNumber, msgErrorEmail, msgErrorName, msgErrorPhone, errorLastname } = this.state;
        console.disableYellowBox = true;
        return (
            <View style={styles.container}>
                <View style={styles.contentHeader}>
                    <View style={styles.contentSignup}>
                        <Text style={styles.text}>Sign Up</Text>
                        <View style={styles.rular} />
                    </View>
                </View>
                <View style={styles.contentMiddle}>
                    <View style={styles.contentInputFieldName}>
                        <View style={styles.contentFieldName}>
                            <Text style={[styles.textLabel, { color: msgErrorName ? 'red' : null }]}>First Name {msgErrorName ? '*' : null}</Text>
                            <TextInput
                                style={[styles.inputStyles, { height: Platform.OS == "ios" ? 50 : null }]}
                                value={firstName}
                                onChangeText={(text) => this.setState({ firstName: text })}
                            />
                        </View>
                        <View style={styles.contentFieldName}>
                            <Text style={[styles.textLabel, { color: errorLastname ? 'red' : null }]}>Last Name {errorLastname ? '*' : null}</Text>
                            <TextInput
                                style={[styles.inputStyles, { height: Platform.OS == "ios" ? 50 : null }]}
                                value={lastName}
                                onChangeText={(text) => this.setState({ lastName: text })}
                            />
                        </View>
                    </View>
                    <View style={styles.marginContent}>
                        <View style={[styles.contentFieldName, { width: '100%' }]}>
                            <Text style={[styles.textLabel, { color: msgErrorEmail ? 'red' : null }]}>Email {msgErrorEmail ? '*' : null} </Text>
                            <TextInput
                                style={[styles.inputStyles, { height: Platform.OS == "ios" ? 50 : null }]}
                                value={email}
                                onChangeText={(text) => this.setState({ email: text })}
                            />
                        </View>
                    </View>

                    <View style={styles.marginContent}>
                        <View style={[styles.contentFieldName, { width: '100%' }]}>
                            <Text style={[styles.textLabel, { color: errorPass ? 'red' : null }]}>password {errorPass ? '*' : null} </Text>
                            <TextInput
                                style={[styles.inputStyles, { height: Platform.OS == "ios" ? 50 : null }]}
                                secureTextEntry={true}
                                value={password}
                                onChangeText={(text) => this.setState({ password: text })}
                            />
                        </View>
                    </View>
                    <View style={styles.marginContent}>
                        <View style={[styles.contentFieldName, { width: '100%' }]}>
                            <Text style={[styles.textLabel, { color: msgErrorPhone ? 'red' : null }]}>Phone Number {msgErrorPhone ? '*' : null}</Text>
                            <TextInput
                                style={[styles.inputStyles, { height: Platform.OS == "ios" ? 50 : null }]}
                                keyboardType='numeric'
                                value={phoneNumber}
                                onChangeText={(number) => this.setState({ phoneNumber: number })}
                            />
                        </View>
                    </View>
                    <View style={[styles.contentInputFieldName, styles.marginContent]}>
                        <View style={styles.contentFieldName}>
                            <Text style={styles.textLabel}>Gender</Text>
                            <View style={styles.contentRadio}>
                                {genders.map((item, index) => {
                                    return (
                                        <TouchableOpacity onPress={() => this._handleGender(index)} style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                            <Icon name={item.status ? 'radio-button-on' : 'radio-button-off'} type='Ionicons' style={{ color: '#1C9CC6' }} />
                                            <Text style={styles.genderText}>{item.gender}</Text>
                                        </TouchableOpacity>
                                    )
                                })}
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.contentFooter}>
                    <TouchableOpacity onPress={this._handleSigup} style={[styles.btnStyles, { marginTop: 5 }]}>
                        <Text style={styles.btnText}>Sign up</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={{ flex: 1 }}>
                        {this.renderInformationInput()}
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        justifyContent: 'space-between',
        height: height
    },
    contentHeader: {
        height: '10%',
        justifyContent: 'center',
    },
    contentMiddle: {
        height: '70%',
        padding: 10
    },
    contentFooter: {
        padding: 10,
        height: '20%',
    },
    contentForgot: {
        marginTop: '15%',
        alignItems: 'center',
    },
    contentSignup: {
        width: '30%',
        alignItems: 'center',
    },
    text: {
        fontSize: 15,
        fontWeight: '700',
        color: '#000'
    },
    rular: {
        padding: 1.5,
        width: '60%',
        backgroundColor: '#1C9CC6',
        marginTop: '5%'
    },
    contentInputFieldName: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    },

    contentFieldName: {
        width: '45%',
    },
    contentInputText: {
        width: '80%'
    },
    hideShowIcon: {
        color: '#b3b3b3',
        fontSize: 20,
    },
    btnEyeIcon: {
        position: 'absolute',
        top: '30%',
        left: '90%'
    },
    inputStyles: {
        fontSize: 15,
        width: '100%',
        borderRadius: 5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,
        borderRadius: 50,
        paddingLeft: 20
    },
    textForgot: {
        fontSize: 15,
        color: '#b3b3b3'
    },
    btnStyles: {
        backgroundColor: '#1C9CC6',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 13,
        borderRadius: 50
    },
    btnText: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 16,
    },
    textLabel: {
        marginBottom: 5
    },
    marginContent: {
        marginTop: 10
    },
    iconStyle: {
        color: '#458cff',
        marginRight: 10
    },
    contentRadio: {
        width: '80%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    genderText: {
        marginLeft: 10,
        fontSize: 15 + 1,
    }
})


export default RegisterScreen
